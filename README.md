# Halloween Lights

A module that adds a string of pumpkin lights to the top of your website.  The Jack-o'-lanterns randomly glow as if lit by a candle.

## Installation

Install this module using the official [Drupal instructions](https://www.drupal.org/node/1897420).

## Configuration

Just enable the module on and the Halloween Lights display on every non-admin page.

## Current Maintainer

[Justin Keiser](https://www.drupal.org/u/keiserjb)

## Similar Projects

Inspired by the [Happy New Year and Merry Christmas!](https://www.drupal.org/project/happy_new_year) module.

Halloween Lights is also available for [Backdrop CMS](https://backdropcms.org/project/halloween_lights).

## License

This project is GPL v2 software. See the LICENSE.txt file in this directory for complete text.
